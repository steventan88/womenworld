
<?php



/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */

//  // Include File

//
// Define
define('WM_TREATMENT','treatment');
define('WM_TESTIMONIAL','testimonial');

// get_template_part( 'includes/custom-shortcode' );

//
function hello_elementor_child_enqueue_scripts() {
	wp_enqueue_style(
		'hello-elementor-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		[
			'hello-elementor-theme-style',
		],
		'1.0.0'
	);
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts' );

// add_shortcode('treatment','treatment_section');

function bootstrapstarter_enqueue_styles() {
    wp_register_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );

    $dependencies = array('bootstrap');
    wp_enqueue_style( 'hello', get_stylesheet_uri(), $dependencies );
}

function bootstrapstarter_enqueue_scripts() {
    $dependencies = array('jquery');
    wp_enqueue_script('bootstrap', get_template_directory_uri().'/assets/js/bootstrap.min.js', $dependencies, '3.3.6', true );
    // wp_enqueue_script('script', get_template_directory_uri().'/assets/js/bootstrap.min.js', $dependencies, '3.3.6', true );
}

add_action( 'wp_enqueue_scripts', 'bootstrapstarter_enqueue_styles' );
add_action( 'wp_enqueue_scripts', 'bootstrapstarter_enqueue_scripts' );

################################################################################
// Add theme support
################################################################################
add_theme_support('nav-menus');
if (function_exists('register_nav_menus')) {
    register_nav_menus(
            array(
                // 'main' => 'Main',
                'mobile-menu' =>'Mobile Menu'
            )
    );
}
