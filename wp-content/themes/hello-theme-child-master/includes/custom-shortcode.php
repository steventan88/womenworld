<!-- Mobile Menu -->
<?php
function mobile_section(){
  ob_start();
  ?>
 <div id="mySidenav" class="sidenav">
   <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
   <?php
  wp_nav_menu( array(
    'theme_location'  => 'mobile-menu',
    // 'menu_id'         => 'primary-menu',
    // 'container'       => 'div',
    // 'container_class' => 'collapse navbar-collapse',
    'container_id'    => 'primary-menu-wrap',
    'menu_class'      => 'navbar-nav ml-auto'
  ));
   ?>
</div>
 <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
 <?php
  return ob_get_clean();
  }
  add_shortcode('mobile','mobile_section');
 ?>
<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>


<!-- Treatment Section -->
<?php
function treatment_section(){
  ob_start();
  ?>
  <section>

  <div class="container-fluid p-0">
  	<div class="row no-gutters">
  		<?php
        $treatment_meta = array(
          'post_type' => WM_TREATMENT,
          'post_status'=> "publish",
          'posts_per_page'=> 4,
          'order'=> 'ASC'
        );
        $treaments = get_posts($treatment_meta);
        foreach ($treaments as $treament) :
          $t_name = $treament->post_title;
          $t_dec = $treament->post_content;
          $t_img = wp_get_attachment_image_src(get_post_thumbnail_id($treament->ID),'full');
      ?>
  			<div class="col-lg-3 col-md-6 col-sm-6 content-wrapper">
          <?php if($t_img): ?>
            <div class="img-box">
              <a href="#">  <img src="<?php echo $t_img[0]; ?>" alt="<?php echo $t_name; ?>" class="img-fluid">
                <div class="content-box">
                    <h3 class="title"><?php echo $t_name; ?></h3>
                </div>
              </a>
            </div>
          <?php endif; ?>
          <div class="portfolio-content">
            <h3 class="entry-title"><?php echo $t_name; ?></h3>
            <div class="sep-dotted"> </div>
            <p><?php echo $t_dec; ?></p>
          </div>
  			</div>
        <?php endforeach; ?>
  	</div>
  </div>
  </section>
<?php
  return ob_get_clean();
  }
  add_shortcode('treatment','treatment_section');
 ?>

<!-- Testomonials -->
<?php
  function testimonials(){
    ob_start();
    $testimon = array(
      'post_type' => WM_TESTIMONIAL,
      'post_status'=> "publish",
      'order'=> "DESC"
    );
    $testimonial = get_posts($testimon);


?>
<div class="swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide">Slide 1</div>
      <div class="swiper-slide">Slide 2</div>
      <div class="swiper-slide">Slide 3</div>
      <div class="swiper-slide">Slide 4</div>
      <div class="swiper-slide">Slide 5</div>
      <div class="swiper-slide">Slide 6</div>
      <div class="swiper-slide">Slide 7</div>
      <div class="swiper-slide">Slide 8</div>
      <div class="swiper-slide">Slide 9</div>
      <div class="swiper-slide">Slide 10</div>
    </div>
    <!-- Add Pagination -->
       <div class="swiper-pagination"></div>
  </div>

  <!-- Initialize Swiper -->
 <script>
   var swiper = new Swiper('.swiper-container', {
     pagination: {
       el: '.swiper-pagination',
     },
   });
 </script>

<?php
  return ob_get_clean();
  }
  add_shortcode( 'testimonial', 'testimonials' )
?>
