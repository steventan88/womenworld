
<!-- Treatment Section -->
<?php
function treatment_section(){
  ob_start();
  ?>
  <div class="container-fluid">
  	<div class="row no-gutters">
  		<?php
        $treatment_meta = array(
          'post_type' => WM_TREATMENT,
          'post_status'=> "publish",
          'posts_per_page'=> 4,
          'order'=> 'ASC'
        );
        $treaments = get_posts($treatment_meta);
        foreach ($treaments as $treament) :
          $t_name = $treament->post_title;
          $t_dec = $treament->post_content;
          $t_img = wp_get_attachment_image_src(get_post_thumbnail_id($treament->ID),'full');
      ?>
  			<div class="col-lg-3 col-md-6 col-sm-6 content-wrapper">
          <?php if($t_img): ?>
            <div class="img-box">
              <a href="#">  <img src="<?php echo $t_img[0]; ?>" alt="<?php echo $t_name; ?>" class="img-fluid">
                <div class="content-box">
                    <h3 class="title"><?php echo $t_name; ?></h3>
                </div>
              </a>
            </div>
          <?php endif; ?>
          <div class="portfolio-content">
            <h3 class="entry-title"><?php echo $t_name; ?></h3>
            <div class="sep-dotted"> </div>
            <p><?php echo $t_dec; ?></p>
          </div>
  			</div>
        <?php endforeach; ?>

  	</div>
  </div>
<?php
  return ob_get_clean();
  }
  add_shortcode('treatment','treatment_section')
 ?>
